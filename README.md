# FrontendMentor Solutions

This is a repository used to center all my solutions for challenges proposed by FrontendMentor, each solution is organized in a dedicated subbranch, with direct links for easy navigation.

# How to Navigate

- The main branch (main) lists all the solutions submitted to [FrontendMentor](https://www.frontendmentor.io/);
- Each solution has a link to its respective subbranch, where you can find project details, including source code, screenshots, and notes;
- Feel free to give me some feedbacks 😉;

# Solutions

- [QR code component solution](https://gitlab.com/jspereiramoura/frontednmentor/-/tree/qr-code-component?ref_type=heads)

# Contact

To get in touch, please send a message through my [GitLab](https://gitlab.com/jspereiramoura) or my [GitHub](https://gitlab.com/jspereiramoura)
